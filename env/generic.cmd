epicsEnvSet("IOCNAME",   "LLRF")
epicsEnvSet("AS_TOP",   "/opt/nonvolatile")
epicsEnvSet("LLRFTYPE",   "generic")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES" "16777300")
epicsEnvSet("LLRF_PREFIX" "LLRF1")
epicsEnvSet("LLRF_DIGRTM_PREFIX" "LLRF")
epicsEnvSet("F-SAMPLING" "96.250")
epicsEnvSet("F-SYSTEM" "352.210")
epicsEnvSet("NEARIQN" "14")
epicsEnvSet("NEARIQM" "3")

epicsEnvSet("LLRF_SLOT_1" "4")

# Move this to llrfsystem with different substitutions?
epicsEnvSet("CAVITYCH" "0")
epicsEnvSet("CAVFWDCH" "2")
epicsEnvSet("PWRFWDCH" "3")
epicsEnvSet("TABLE_SMNM_MAX" "1000") # Double check this
