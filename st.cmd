require common

epicsEnvSet("ENGINEER", "Gabriel Fedel <gabriel.fedel@ess.eu>"
## Add extra environment variables here

epicsEnvSet("TOP",      "$(E3_CMD_TOP)")


# Load standard module startup scripts
#iocshLoad("$(common_DIR)/e3-common.iocsh")

require sis8300llrf
require llrfsystem
ndsSetTraceLevel 2

dbl > PVs.list
## For commands to be run after iocInit, use the function afterInit()

# Call iocInit to start the IOC
iocInit()
date
